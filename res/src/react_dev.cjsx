class CommentBox extends React.Component

  constructor: ->
    super()

    @state =
      a_comments: []
      b_show_comments: true

  load_from_server: ->
    if @state.b_show_comments is false
      return
    $.ajax
      url: 'http://www.horariosytiendas-es.loc/api/test'
      dataType: 'json'
      cache: false
      success: ((data) ->
        @setState({a_comments: data})
        return
      ).bind(this)
      error: ((xhr, status, err) ->
        console.error '/api/test', status, err.toString()
        return
      ).bind(this)

  # predefinida
  componentWillMount: ->
    @load_from_server()
    @x_timer = setInterval( () =>
      @load_from_server()
    , 5000)

  # predefinida
  componentWillUnmount: ->
    console.log "unmount!!!"
    clearInterval(@x_timer)

  # componentDidMount: ->
  #   @load_from_server()
  #   setInterval(@load_from_server, @props.poll_interval)


  get_comments: ->
    @state.a_comments.map( (o_comm) ->
      # console.log "eeeee:", o_comm.author
      <Comment data={o_comm} />
    )

  add_comment: ({s_author, s_text}) ->

    o_comm = {
      id: (@state.a_comments.length + 1)
      author: s_author
      text: s_text
    }

    $.ajax
      url: 'http://www.horariosytiendas-es.loc/api/testop'
      method: 'POST'
      data: JSON.stringify({comment: o_comm})
      contentType: 'application/json'
      # Type: 'json'
      cache: false
      success: ((data) ->
        console.log "RECIBIDO:", data
        @setState({a_comments: data})
        return
      ).bind(this)
      error: ((xhr, status, err) ->
        console.error '/api/test', status, err.toString()
        return
      ).bind(this)
    # @setState({a_comments: @state.a_comments.concat([o_comm])})

  handle_show_hide_click: (b_show_or_hide_comms) ->
    console.log "clickeado #{b_show_or_hide_comms}"
    @setState({b_show_comments: b_show_or_hide_comms })

  render: ->
    a_comment_nodes = []
    # s_button_text = "Show Comments"

    if @state.b_show_comments
      # s_button_text = "Hide Comments"
      a_comment_nodes = @get_comments()

    <div className="commentBox">
      <ShowHideButton set_show_comments_prop={@handle_show_hide_click.bind(this)}/>
      <h2>Comments</h2>
      <CommentList data={a_comment_nodes} />
      <CommentForm add_comment_prop={@add_comment.bind(this)} />
    </div>


class ShowHideButton extends React.Component
  constructor: ->
    super()
    @s_button_text = ""
    @a_texts = ["Show Comments", "Hide Comments"]
    @state = {
      b_comments_on: false
    }

  handle_click: ->
    console.log "clickeado show hide button"
    @setState({b_comments_on: not @state.b_comments_on})
    @props.set_show_comments_prop(not @state.b_comments_on)

  render: ->
    @s_button_text = @a_texts[(if @state.b_comments_on then 1 else 0)]
    <button onClick={@handle_click.bind(this)} >{@s_button_text}</button>

class CommentList extends React.Component
  render: ->
    # comment_nodes = @props.data.map( (o_comm) ->
    #   <Comment data={o_comm}/>
    # )
    <ul className="commentList">
      {@props.data}
    </ul>

class CommentForm extends React.Component
  handle_submit: (e) ->
    e.preventDefault()
    o_comm = {s_author: @author_input.value, s_text: @body_input.value}
    $('#comment_input').val('')
    @props.add_comment_prop(o_comm) # se lo pasa al binding

  render: ->
    <form className="commentForm" onSubmit={@handle_submit.bind(this)}>
        <input type="text" placeholder="Your name" ref={ (input) => @author_input = input} />
        <input type="text" id="comment_input" placeholder="Say something..." ref={(input) => @body_input = input} />
        <input type="submit" value="Post" />
    </form>

class Comment extends React.Component
  constructor: ->
    super()
  render: ->
    <li className="comment" key={@props.data.id} >
      <strong  className="commentAuthor">{@props.data.author}</strong>: <span>{@props.data.text}</span>
    </li>

# data={a_authors}
ReactDOM.render(
  # <CommentBox url="/api/test" poll_interval={2000} />
  <CommentBox />
  document.getElementById 'content'
)
