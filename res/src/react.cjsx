React = require("react")
ReactDOM = require("react-dom")
jQuery = require("jquery")
# CSS

# Comment Box (main container)
class CommentBox extends React.Component

  constructor: ->
    super()

    @state =
      a_comments: []
      b_show_comments: true

  # predefinida
  componentWillMount: ->
    @._fetch_comments()

  componentDidMount: ->
    #@._timer = setInterval(=>
#        @._fetch_comments()
#    , 5000)

  # predefinida
  componentWillUnmount: ->
    clearInterval(@._timer)

  _fetch_comments: ->
    jQuery.ajax({
        url: '/api/comments'
        method: 'GET'
        success: (a_comments) =>
            @.setState({ a_comments: a_comments})
    })

  _delete_comment: (o_comment) ->
    jQuery.ajax({
        url: "/api/comments/#{o_comment.id}"
        method: 'DELETE'
    })

    a_comments = [@.state.a_comments...]
    n_index = a_comments.indexOf(o_comment)
    a_comments.splice(n_index, 1)

    @.setState({ a_comments })

  _get_comments: ->
    console.log 'getting comments!'

    return @.state.a_comments.map((o_comment) =>
        <Comment
            author={o_comment.author}
            body={o_comment.body}
            key={o_comment.id}
            comment={o_comment}
            on_delete={@._delete_comment.bind(@)}
        />
    )

  _get_comments_title: (n_count) ->
    if n_count is 0
      'No comments yet'
    else if n_count is 1
        '1 comment'
    else
      "#{n_count} comments"

  _handle_click: ->
    @.setState({ b_show_comments: !@state.b_show_comments })

  _add_comment: (s_author, s_body) ->
    o_comment =
        id: @.state.a_comments.length + 1
        author: s_author
        body: s_body

    jQuery.ajax({
        url: "/api/comments"
        method: "POST"
        data: { comment: o_comment }
        success: =>
            @.setState({ a_comments: @.state.a_comments.concat([o_comment]) })
    })

  render: ->
    a_comments = @._get_comments()

    s_button_text = 'Show comments'
    if @state.b_show_comments
        s_button_text = 'Hide comments'
        s_comments_node = <div className="comment-list">{a_comments}</div>

    <div className="comment-box">
      <CommentForm add_comment={@._add_comment.bind(@)} />

      <h4>{@._get_comments_title(a_comments.length)}</h4>
      <button onClick={@._handle_click.bind(@)}>{s_button_text}</button>
      {s_comments_node}
    </div>


# Single comment
class Comment extends React.Component
    _handle_delete: (e) ->
        e.preventDefault()

        if confirm('Sure?')
            @.props.on_delete(@.props.comment)

    render: ->
        <div id={"comment-#{@props.id}"} className="comment">
            <p className="comment-header">{@props.author}</p>
            <p className="comment-body">{@props.body}</p>
            <div className="comment-footer">
                <a href="#" onClick={@._handle_delete.bind(@)} className="comment-footer-delete">Delete comment</a>
            </div>
        </div>


# Comment form to allow users adding new comments
class CommentForm extends React.Component
    _handle_submit: (e) ->
        e.preventDefault()
        console.log 'submiting:'
        @.props.add_comment(@._author.value, @._body.value)

    render: ->
        <form className="comment-form" onSubmit={@._handle_submit.bind(@)}>
            <label>Join the discussion</label>
            <div className="comment-form-fields">
                <input placeholder="Name:" ref={(input) => @._author = input}/>
                <textarea placeholder="Comment:" ref={(textarea) => @._body = textarea}></textarea>
            </div>
            <div className="comment-form-actions">
                <button type="submit">Post comment</button>
            </div>
        </form>

# Trigger
ReactDOM.render(
  <CommentBox />
  document.getElementById 'form'
)
