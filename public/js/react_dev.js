var Comment, CommentBox, CommentForm, CommentList, ShowHideButton,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

CommentBox = (function(_super) {
  __extends(CommentBox, _super);

  function CommentBox() {
    CommentBox.__super__.constructor.call(this);
    this.state = {
      a_comments: [],
      b_show_comments: true
    };
  }

  CommentBox.prototype.load_from_server = function() {
    if (this.state.b_show_comments === false) {
      return;
    }
    return $.ajax({
      url: 'http://www.horariosytiendas-es.loc/api/test',
      dataType: 'json',
      cache: false,
      success: (function(data) {
        this.setState({
          a_comments: data
        });
      }).bind(this),
      error: (function(xhr, status, err) {
        console.error('/api/test', status, err.toString());
      }).bind(this)
    });
  };

  CommentBox.prototype.componentWillMount = function() {
    this.load_from_server();
    return this.x_timer = setInterval((function(_this) {
      return function() {
        return _this.load_from_server();
      };
    })(this), 5000);
  };

  CommentBox.prototype.componentWillUnmount = function() {
    console.log("unmount!!!");
    return clearInterval(this.x_timer);
  };

  CommentBox.prototype.get_comments = function() {
    return this.state.a_comments.map(function(o_comm) {
      return React.createElement(Comment, {
        "data": o_comm
      });
    });
  };

  CommentBox.prototype.add_comment = function(_arg) {
    var o_comm, s_author, s_text;
    s_author = _arg.s_author, s_text = _arg.s_text;
    o_comm = {
      id: this.state.a_comments.length + 1,
      author: s_author,
      text: s_text
    };
    return $.ajax({
      url: 'http://www.horariosytiendas-es.loc/api/testop',
      method: 'POST',
      data: JSON.stringify({
        comment: o_comm
      }),
      contentType: 'application/json',
      cache: false,
      success: (function(data) {
        console.log("RECIBIDO:", data);
        this.setState({
          a_comments: data
        });
      }).bind(this),
      error: (function(xhr, status, err) {
        console.error('/api/test', status, err.toString());
      }).bind(this)
    });
  };

  CommentBox.prototype.handle_show_hide_click = function(b_show_or_hide_comms) {
    console.log("clickeado " + b_show_or_hide_comms);
    return this.setState({
      b_show_comments: b_show_or_hide_comms
    });
  };

  CommentBox.prototype.render = function() {
    var a_comment_nodes;
    a_comment_nodes = [];
    if (this.state.b_show_comments) {
      a_comment_nodes = this.get_comments();
    }
    return React.createElement("div", {
      "className": "commentBox"
    }, React.createElement(ShowHideButton, {
      "set_show_comments_prop": this.handle_show_hide_click.bind(this)
    }), React.createElement("h2", null, "Comments"), React.createElement(CommentList, {
      "data": a_comment_nodes
    }), React.createElement(CommentForm, {
      "add_comment_prop": this.add_comment.bind(this)
    }));
  };

  return CommentBox;

})(React.Component);

ShowHideButton = (function(_super) {
  __extends(ShowHideButton, _super);

  function ShowHideButton() {
    ShowHideButton.__super__.constructor.call(this);
    this.s_button_text = "";
    this.a_texts = ["Show Comments", "Hide Comments"];
    this.state = {
      b_comments_on: false
    };
  }

  ShowHideButton.prototype.handle_click = function() {
    console.log("clickeado show hide button");
    this.setState({
      b_comments_on: !this.state.b_comments_on
    });
    return this.props.set_show_comments_prop(!this.state.b_comments_on);
  };

  ShowHideButton.prototype.render = function() {
    this.s_button_text = this.a_texts[(this.state.b_comments_on ? 1 : 0)];
    return React.createElement("button", {
      "onClick": this.handle_click.bind(this)
    }, this.s_button_text);
  };

  return ShowHideButton;

})(React.Component);

CommentList = (function(_super) {
  __extends(CommentList, _super);

  function CommentList() {
    return CommentList.__super__.constructor.apply(this, arguments);
  }

  CommentList.prototype.render = function() {
    return React.createElement("ul", {
      "className": "commentList"
    }, this.props.data);
  };

  return CommentList;

})(React.Component);

CommentForm = (function(_super) {
  __extends(CommentForm, _super);

  function CommentForm() {
    return CommentForm.__super__.constructor.apply(this, arguments);
  }

  CommentForm.prototype.handle_submit = function(e) {
    var o_comm;
    e.preventDefault();
    o_comm = {
      s_author: this.author_input.value,
      s_text: this.body_input.value
    };
    $('#comment_input').val('');
    return this.props.add_comment_prop(o_comm);
  };

  CommentForm.prototype.render = function() {
    return React.createElement("form", {
      "className": "commentForm",
      "onSubmit": this.handle_submit.bind(this)
    }, React.createElement("input", {
      "type": "text",
      "placeholder": "Your name",
      "ref": ((function(_this) {
        return function(input) {
          return _this.author_input = input;
        };
      })(this))
    }), React.createElement("input", {
      "type": "text",
      "id": "comment_input",
      "placeholder": "Say something...",
      "ref": ((function(_this) {
        return function(input) {
          return _this.body_input = input;
        };
      })(this))
    }), React.createElement("input", {
      "type": "submit",
      "value": "Post"
    }));
  };

  return CommentForm;

})(React.Component);

Comment = (function(_super) {
  __extends(Comment, _super);

  function Comment() {
    Comment.__super__.constructor.call(this);
  }

  Comment.prototype.render = function() {
    return React.createElement("li", {
      "className": "comment",
      "key": this.props.data.id
    }, React.createElement("strong", {
      "className": "commentAuthor"
    }, this.props.data.author), ": ", React.createElement("span", null, this.props.data.text));
  };

  return Comment;

})(React.Component);

ReactDOM.render(React.createElement(CommentBox, null), document.getElementById('content'));
