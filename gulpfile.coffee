# Load all required libraries.
gulp = require 'gulp'
path = require 'path'
gutil = require 'gulp-util'
coffee_react = require 'gulp-coffee-react'

# cjsx [options] path/to/script.cjsx -- [args]

gulp.task('coffee-cjsx', ->
  gulp.src('./res/src/*.cjsx')
    .pipe(coffee_react({bare: true}))
    .on('error', gutil.log)
    .pipe(gulp.dest('./public/js'))
)

gulp.task 'watch', ->
  gulp.watch('./res/src/*.cjsx', ['coffee-cjsx'])


# Default task call every tasks created so far.
gulp.task('default', ['coffee-cjsx', 'watch'])
