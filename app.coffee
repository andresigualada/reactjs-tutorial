o_express = require 'express'
o_app = o_express()
o_app.disable('x-powered-by') # para que no se vea x-powered-by express en las cabeceras
o_app.disable('etag') # evitamos que se cacheen los archivos
o_app.enable('trust proxy') # para saber si está detrás de un proxy

# Body parser (para poder parsear el cuerpo de la respuesta en las llamadas ajax)
o_bparser = require 'body-parser'
o_app.use(o_bparser.json()) # support json encoded bodies
o_app.use(o_bparser.urlencoded({ extended: true })) # support encoded bodies

# directorio para los archivos de img, css y js
o_app.use(o_express.static("#{__dirname}/public"))

Router = require './routes/Router'
o_routes = new Router()

o_app.use('/', o_routes.o_router, (err, res) ->
  if err
    res.status(404).send("404")
)

# server levantado en el puerto X
server = o_app.listen(5000, 'localhost', ->
  s_host = server.address().address
  n_port = server.address().port
  console.log "Listening at #{s_host}:#{n_port}"
)

# si el proceso termina cerramos moongose
process.on('SIGINT', ->
  process.exit(0)
)