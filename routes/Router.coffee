o_express = require('express')

class Router

  constructor: ->
    @a_comments = [
      { id:1, author: 'Andrés', body: 'Test comment 1' }
      { id:2, author: 'Omer', body: 'Test comment 2' }
      { id:3, author: 'Pepe', body: 'Test comment 3' }
    ]

    @o_router = o_express.Router()

    @set_routes()

  set_routes: ->
    @o_router.get('/api/comments', (req, res, next) =>
      return res.json(@a_comments)
    )

    @o_router.post('/api/comments', (req, res, next) =>
      @a_comments = @a_comments.concat([req.body.comment])
#      console.log @a_comments
      return res.json({})
    )

    @o_router.delete('/api/comments/:id', (req, res, next) =>
      n_id = parseInt(req.params.id)
      @a_comments = @a_comments.filter((o_comm) =>
        parseInt(o_comm.id) isnt n_id
      )

      return res.json(@a_comments)
    )


module.exports = Router