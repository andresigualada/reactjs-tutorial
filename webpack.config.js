var precss = require('precss');
var autoprefixer = require('autoprefixer');

module.exports = {
    entry: "./entry.js",
    output: {
        path: __dirname + '/public/js/',
        filename: "bundle.js"
    },
    module: {
        loaders: [
            { test: /\.cjsx$/, loader: "coffee-jsx-loader" },
            {
                test:   /\.css$/,
                loader: "style-loader!css-loader!postcss-loader"
            }
        ]
    },
    postcss: function () {
        return [precss, autoprefixer];
    }
};