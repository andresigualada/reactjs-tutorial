var Comment, CommentBox, CommentForm,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  __slice = [].slice;

CommentBox = (function(_super) {
  __extends(CommentBox, _super);

  function CommentBox() {
    CommentBox.__super__.constructor.call(this);
    this.state = {
      a_comments: [],
      b_show_comments: true
    };
  }

  CommentBox.prototype.componentWillMount = function() {
    return this._fetch_comments();
  };

  CommentBox.prototype.componentDidMount = function() {};

  CommentBox.prototype.componentWillUnmount = function() {
    return clearInterval(this._timer);
  };

  CommentBox.prototype._fetch_comments = function() {
    return jQuery.ajax({
      url: '/api/comments',
      method: 'GET',
      success: (function(_this) {
        return function(a_comments) {
          return _this.setState({
            a_comments: a_comments
          });
        };
      })(this)
    });
  };

  CommentBox.prototype._delete_comment = function(o_comment) {
    var a_comments, n_index;
    jQuery.ajax({
      url: "/api/comments/" + o_comment.id,
      method: 'DELETE'
    });
    a_comments = __slice.call(this.state.a_comments);
    n_index = a_comments.indexOf(o_comment);
    a_comments.splice(n_index, 1);
    return this.setState({
      a_comments: a_comments
    });
  };

  CommentBox.prototype._get_comments = function() {
    console.log('getting comments!');
    return this.state.a_comments.map((function(_this) {
      return function(o_comment) {
        return React.createElement(Comment, {
          "author": o_comment.author,
          "body": o_comment.body,
          "key": o_comment.id,
          "comment": o_comment,
          "on_delete": _this._delete_comment.bind(_this)
        });
      };
    })(this));
  };

  CommentBox.prototype._get_comments_title = function(n_count) {
    if (n_count === 0) {
      return 'No comments yet';
    } else if (n_count === 1) {
      return '1 comment';
    } else {
      return "" + n_count + " comments";
    }
  };

  CommentBox.prototype._handle_click = function() {
    return this.setState({
      b_show_comments: !this.state.b_show_comments
    });
  };

  CommentBox.prototype._add_comment = function(s_author, s_body) {
    var o_comment;
    o_comment = {
      id: this.state.a_comments.length + 1,
      author: s_author,
      body: s_body
    };
    return jQuery.ajax({
      url: "/api/comments",
      method: "POST",
      data: {
        comment: o_comment
      },
      success: (function(_this) {
        return function() {
          return _this.setState({
            a_comments: _this.state.a_comments.concat([o_comment])
          });
        };
      })(this)
    });
  };

  CommentBox.prototype.render = function() {
    var a_comments, s_button_text, s_comments_node;
    a_comments = this._get_comments();
    s_button_text = 'Show comments';
    if (this.state.b_show_comments) {
      s_button_text = 'Hide comments';
      s_comments_node = React.createElement("div", {
        "className": "comment-list"
      }, a_comments);
    }
    return React.createElement("div", {
      "className": "comment-box"
    }, React.createElement(CommentForm, {
      "add_comment": this._add_comment.bind(this)
    }), React.createElement("h4", null, this._get_comments_title(a_comments.length)), React.createElement("button", {
      "onClick": this._handle_click.bind(this)
    }, s_button_text), s_comments_node);
  };

  return CommentBox;

})(React.Component);

Comment = (function(_super) {
  __extends(Comment, _super);

  function Comment() {
    return Comment.__super__.constructor.apply(this, arguments);
  }

  Comment.prototype._handle_delete = function(e) {
    e.preventDefault();
    if (confirm('Sure?')) {
      return this.props.on_delete(this.props.comment);
    }
  };

  Comment.prototype.render = function() {
    return React.createElement("div", {
      "id": "comment-" + this.props.id,
      "className": "comment"
    }, React.createElement("p", {
      "className": "comment-header"
    }, this.props.author), React.createElement("p", {
      "className": "comment-body"
    }, this.props.body), React.createElement("div", {
      "className": "comment-footer"
    }, React.createElement("a", {
      "href": "#",
      "onClick": this._handle_delete.bind(this),
      "className": "comment-footer-delete"
    }, "Delete comment")));
  };

  return Comment;

})(React.Component);

CommentForm = (function(_super) {
  __extends(CommentForm, _super);

  function CommentForm() {
    return CommentForm.__super__.constructor.apply(this, arguments);
  }

  CommentForm.prototype._handle_submit = function(e) {
    e.preventDefault();
    console.log('submiting:');
    return this.props.add_comment(this._author.value, this._body.value);
  };

  CommentForm.prototype.render = function() {
    return React.createElement("form", {
      "className": "comment-form",
      "onSubmit": this._handle_submit.bind(this)
    }, React.createElement("label", null, "Join the discussion"), React.createElement("div", {
      "className": "comment-form-fields"
    }, React.createElement("input", {
      "placeholder": "Name:",
      "ref": ((function(_this) {
        return function(input) {
          return _this._author = input;
        };
      })(this))
    }), React.createElement("textarea", {
      "placeholder": "Comment:",
      "ref": ((function(_this) {
        return function(textarea) {
          return _this._body = textarea;
        };
      })(this))
    })), React.createElement("div", {
      "className": "comment-form-actions"
    }, React.createElement("button", {
      "type": "submit"
    }, "Post comment")));
  };

  return CommentForm;

})(React.Component);

ReactDOM.render(React.createElement(CommentBox, null), document.getElementById('form'));
